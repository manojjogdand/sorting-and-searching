#include<iostream>
#include<string>
#include<iomanip>
int pass=0,comparisons=0;
//type alias
//using functionpointer=bool(*)(int,int);
//functionpointersort_fun;
typedef bool(*functionpointer)(int,int);
functionpointer sort_fun;
using namespace std;
class Exception
{
private:
	string message;
public:
	Exception(string message=" ")
{
		this->message=message;
}
	void printmessage(void)
	{
		cerr<<message;
	}

};
int* allocatememory(int size)
{
	return new int[size];

}

bool Dealloactememory(int *arr)
{

	if(arr!=NULL)
	{
		delete []arr;
		arr=NULL;
		return true;
	}
	else
		return false;

}
void acceptarray(int *arr,int size)
{
	cout<<endl<<"Enter array elements   :"<<endl;
	for(int index=0;index<size;index++)
	{
		cout<<"arr["<<index<<"]:";
		cin>>arr[index];
	}
}
void displayarray(int* arr,int size)
{

	cout<<endl<<"Array elements are :";

	for(int index=0;index<size;index++)
	{

		cout<<setw(4)<<left<<arr[index];
	}
}
int linearsearch(int *arr,int size)throw(Exception)
		{
	cout<<endl<<"Enter key   :";
	int key;
	cin>>key;
	for(int index=0;index<size;index++)
	{
		if(key==arr[index])
			return index;
	}
	throw Exception("Element is not present in the array");

		}

int recursivelinearsearch(int *arr,int index,int size,int key)throw(Exception)
		{

	if(index==size)
		throw Exception("Element is not present in the array");
	if(key==arr[index])
		return index;
	recursivelinearsearch(arr, ++index,size,key);
		}


int binarysearch(int* arr,int size)throw(Exception)
		{
	cout<<endl<<"Enter key   :";
	int key;
	cin>>key;
	int start=0;
	int end=size-1;

	while(start<=end)
	{
		int mid=(start+end)/2;

		if(key==arr[mid])
			return mid;
		if(key<arr[mid])
			end=mid-1;
		else
			start=mid+1;


	}
	throw Exception("Element is not present in the array");

		}
int recursivebinarysearch(int* arr,int start,int end,int key)
{
	if(start > end)
		throw Exception("Element is not present in the array");

	int mid=(start+end)/2;
	if(key==arr[mid])
		return mid;
	if(key<arr[mid])
		recursivebinarysearch(arr,start,mid-1,key);
	else
		recursivebinarysearch(arr,mid+1,end,key);



}
bool ascending_order(int x,int y)
{
	if(x>y)
		return true;
	else
		return false;
}
bool descending_order(int x,int y)
{
	if(x<y)
		return true;
	else
		return false;
}

functionpointer sortingorders(void)
{

	int choice;
	cout<<endl<<"1.ASCENDING ORDER"<<endl<<"2.DESCENDING ORDER"<<endl<<"DEFAULT SET TO ASCENDING ORDER "<<endl;
	cout<<endl<<"Enter your choice   :";
	cin>>choice;
	switch(choice)
	{
	case 1:
		return ascending_order;

	case 2:
		return descending_order;

	default:
		return ascending_order;
	}


}



void selection_sort(int arr[],int size)
{
	sort_fun=::sortingorders();

	for(int sel_pos=0;sel_pos<size-1;sel_pos++)
	{
		cout<<endl<<"For pass  :"<<++pass;

		for(int pos=sel_pos+1;pos<size;pos++)
		{
			comparisons++;
			if(sort_fun(arr[sel_pos],arr[pos]))
			{ swap(arr[sel_pos],arr[pos]);
			::displayarray(arr,size);
			}
		}


	}

	cout<<endl<<"Number of iterations/passes   :"<<pass<<endl<<"Number of Comparisons  :"<<comparisons;


}
void Bubble_sort(int arr[],int size)
{
	sort_fun=::sortingorders();
	comparisons=0,pass=0;
	for(int iteration=0;iteration<size-1;iteration++)
	{
		cout<<endl<<"For pass  :"<<++pass;
		for(int pos=0;pos<size-1-iteration;pos++)
		{
			comparisons++;
			if(sort_fun(arr[pos],arr[pos+1]))
			{
				swap(arr[pos],arr[pos+1]);
				::displayarray(arr,size);
			}
		}

	}
	cout<<endl<<"Number of iterations/passes   :"<<pass<<endl<<"Number of Comparisons  :"<<comparisons;

}
void updated_Bubble_sort(int arr[],int size)
{
	sort_fun=::sortingorders();
	comparisons=0,pass=0;
	bool flag=true;
	for(int iteration=0;iteration<size-1 && flag==true;iteration++)
	{
		flag=false;
		cout<<endl<<"For pass  :"<<++pass;
		for(int pos=0;pos<size-1-iteration;pos++)
		{
			comparisons++;
			if(sort_fun(arr[pos],arr[pos+1]))
			{
				flag=true;
				swap(arr[pos],arr[pos+1]);
				::displayarray(arr,size);
			}
		}

	}
	cout<<endl<<"Number of iterations/passes   :"<<pass<<endl<<"Number of Comparisons  :"<<comparisons;

}
void insertion_sort(int arr[],int size)
{    sort_fun=::sortingorders();
pass=0;
comparisons=0;

for(int sel_pos=1;sel_pos<size;sel_pos++)
{
	cout<<endl<<"For pass  :"<<++pass;
	int key=arr[sel_pos];

	int j=sel_pos-1;
	/*for( ;j>=0 && sort_fun(key,arr[j]);--j)
												{
													comparisons++;
													arr[j+1]=arr[j];
													::displayarray(arr,size);
												}*/
	while(j>=0 && sort_fun(key,arr[j]))
	{
		comparisons++;
		arr[j+1]=arr[j];
		::displayarray(arr,size);
		j--;
	}
	arr[j+1]=key;

}
cout<<endl<<"Number of iterations/passes   :"<<pass<<endl<<"Number of Comparisons  :"<<comparisons;



}

int menu_list(void)
{
	cout<<endl<<endl<<"0.Exit";
	cout<<endl<<"1.ACCEPT ARRAY ";
	cout<<endl<<"2.DISPLAY ARRAY ";
	cout<<endl<<"3.BY LINEAR SEARCH";
	cout<<endl<<"4.BY BINARY SEARCH ";
	cout<<endl<<"5.BY RECURSIVE LINEAR SEARCH";
	cout<<endl<<"6.BY RECURSIVE BINARY  SEARCH";
	cout<<endl<<"7.BY SELECTION SORT ";
	cout<<endl<<"8.BY BUBBLE SORT ";
	cout<<endl<<"9.BY UPDATED_BUBBLE_SORT";
	cout<<endl<<"10.BY INSERTION SORT";
	cout<<endl<<"Enter your choice";
	int choice;
	cin>>choice;
	return choice;
}

int main(void)
{
	cout<<"Enter size of array:";
	int size,choice,key;
	cin>>size;
	//first allocate memory
	int *arr=::allocatememory(size);
	while((choice=::menu_list())!=0)
	{

		try
		{
			switch(choice)
			{
			case 1:
				//accept array elements
				::acceptarray(arr,size);
				break;
			case 2:
				//display array elements
				::displayarray(arr,size);
				break;
			case 3:
				//sort array elements by linear search
				cout<<endl<<"By using linear search  :";
				cout<<endl<<"Element is found at index   :"<<::linearsearch(arr,size);
				break;
			case 4:
				cout<<endl<<"By using binary search  :";
				cout<<endl<<"Element is found at index   :"<<::binarysearch(arr,size);
				break;
			case 5:
				//sort array elements by recursive linear search
				cout<<endl<<"By using recursive linear search  :";
				cout<<endl<<"Enter key   :";
				cin>>key;
				cout<<endl<<"Element is found at index   :"<<::recursivelinearsearch(arr,0,size,key);
				break;
			case 6:
				//sort array elements by recursive linear search
				cout<<endl<<"By using recursive binary  search  :";
				cout<<endl<<"Enter key   :";
				cin>>key;
				cout<<endl<<"Element is found at index   :"<<::recursivebinarysearch(arr,0,size-1,key);
				break;
			case 7:
				cout<<endl<<"By using selection sort  :";
				::selection_sort(arr,size);
				break;
			case 8:
				cout<<endl<<"By using bubble  sort  :";
				::Bubble_sort(arr,size);
				break;
			case 9:
				cout<<endl<<"By using  updated bubble  sort  :";
				::updated_Bubble_sort(arr,size);
				break;
			case 10:
				cout<<endl<<"By using  insertion  sort  :";
				::insertion_sort(arr,size);
				break;




			}
		}
		catch(Exception &ex)
		{
			ex.printmessage();
		}

	}

	if(::Dealloactememory(arr))
		cerr<<endl<<"Memory is Released  ";
	else
		cerr<<endl<<"Memory is not allocated for array";


	return 0;
}
